import W9InfoView from './w9InfoView';

export default class W9InfoViewFactory {

    /**
     * @param {object} data
     * @returns {W9InfoView}
     */
    static construct(data):W9InfoView {

        const id = data.id;

        const isExists = data.isExists;

        return new W9InfoView(
            id,
            isExists
        );

    }

}
