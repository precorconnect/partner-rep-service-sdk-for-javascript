import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import EmailPartnerRepReq from './emailPartnerRepReq';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class SendEmailToPartnerRepFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * sends a email for different SAPNumber.
     * @param {EmailPartnerRepReq} request
     * @param {string} accessToken
     * @returns {Promise}
     */
    execute(request:EmailPartnerRepReq,
            accessToken:string):Promise<string> {

        return this._httpClient
            .createRequest(`partner-reps/emailtopartnerrep`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response =>  response.content);
    }
}

export default SendEmailToPartnerRepFeature;