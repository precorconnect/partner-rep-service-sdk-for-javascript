export default class UpdatePartnerRepPhoneNumberReq {

    _id:number;

    _phoneNumber:string;

    /**
     * @param {number} id
     * @param {string} phoneNumber
     */
    constructor(id:number,
                phoneNumber:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!phoneNumber) {
            throw new TypeError('phoneNumber required');
        }
        this._phoneNumber = phoneNumber;

    }


    get id():number {
        return this._id;
    }

    get phoneNumber():string {
        return this._phoneNumber;
    }

    toJSON() {
        return {
            id: this._id,
            phoneNumber: this._phoneNumber
        };
    }
}