/**
 * The least detailed view of a partner rep
 * @class {PartnerRepSynopsisView}
 */
export default class PartnerRepSynopsisView {

    _firstName:string;

    _lastName:string;

    _emailAddress:string;

    _id:string;

    _sapAccountNumber:string;

    _groupId:string;

    constructor(firstName:string,
                lastName:string,
                emailAddress:string,
                id:string,
                sapAccountNumber:string,
                groupId:string) {

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!emailAddress) {
            throw new TypeError('emailAddress required');
        }
        this._emailAddress = emailAddress;

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!sapAccountNumber) {
            throw new TypeError('sapAccountNumber required');
        }
        this._sapAccountNumber = sapAccountNumber;

        if (!groupId) {
            throw new TypeError('groupId required');
        }
        this._groupId = groupId;
    }

    get firstName():string {
        return this._firstName;
    }

    get lastName():string {
        return this._lastName;
    }

    get emailAddress():string {
        return this._emailAddress;
    }

    get id():string {
        return this._id;
    }

    get sapAccountNumber():string {
        return this._sapAccountNumber;
    }

    get groupId():string {
        return this._groupId;
    }

    toJSON() {
        return {
            firstName: this._firstName,
            lastName: this._lastName,
            emailAddress: this._emailAddress,
            id: this._id,
            sapAccountNumber: this._sapAccountNumber,
            groupId: this._groupId
        };
    }

}
