import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AdminEmailPartnerRepReq from './adminEmailPartnerRepReq';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class SendEmailDifferentSAPAccountNumberFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * sends a email for different SAPNumber.
     * @param {AdminEmailPartnerRepReq} request
     * @param {string} accessToken
     * @returns {Promise.<string>}
     */
    execute(request:AdminEmailPartnerRepReq,
            accessToken:string):Promise<string> {

        return this._httpClient
            .createRequest(`partner-reps/emailtoadmin`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response =>  response.content);
    }
}

export default SendEmailDifferentSAPAccountNumberFeature;