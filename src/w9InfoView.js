
/**
 * The most detailed view of a w9 info
 * @class {W9InfoView}
 */
export default class W9InfoView {

    _id:string;

    _isExists:boolean;


    /**
     *
     * @param {string} id
     * @param {boolean} isExists
     */
    constructor(id:string,
                isExists:boolean
    ) {

        this._id = id;

        this._isExists = isExists;

    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {boolean}
     */
    get isExists():boolean {
        return this._isExists;
    }


    toJSON() {
        return {
            id: this._id,
            isExists: this._isExists
        }
    }
};