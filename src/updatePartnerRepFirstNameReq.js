export default class UpdatePartnerRepFirstNameReq {

    _id:number;

    _firstName:string;

    /**
     * @param {number} id
     * @param {string} firstName
     */
    constructor(id:number,
                firstName:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

    }


    get id():number {
        return this._id;
    }

    get firstName():string {
        return this._firstName;
    }

    toJSON() {
        return {
            id: this._id,
            firstName: this._firstName
        };
    }
}