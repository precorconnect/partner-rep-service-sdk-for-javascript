Feature: Update Partner Rep Postal Address
  Updates the postal address of the partner rep with the provided id

  Background:
    Given a postalAddress consists of:
      | attribute                 | validation | type   |
      | street                    | required   | string |
      | city                      | required   | string |
      | regionIso31662Code        | required   | string |
      | postalCode                | required   | string |
      | countryIso31661Alpha2Code | required   | string |

  Scenario: Success
    Given I provide an accessToken identifying me as the partnerRep with id partnerRepId
    And I provide a valid postalAddress
    When I execute updatePartnerRepPostalAddress
    Then my postalAddress is updated in the partner-rep-service
