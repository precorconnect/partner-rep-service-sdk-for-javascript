Feature: Update Partner Rep Last Name
  Updates the last name of the partner rep with the provided id


  Scenario: Success
    Given I provide an accessToken identifying me as the partnerRep with id partnerRepId
    And I provide a valid lastName
    When I execute updatePartnerRepLastName
    Then my lastName is updated in the partner-rep-service