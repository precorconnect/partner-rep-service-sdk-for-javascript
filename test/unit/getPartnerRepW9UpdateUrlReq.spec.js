import GetPartnerRepW9UpdateUrlReq from '../../src/getPartnerRepW9UpdateUrlReq';
import dummy from '../dummy';

/*
 tests
 */
describe('GetPartnerRepW9UpdateUrlReq class', () => {
    describe('constructor', () => {
        it('throws if partnerRepId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new GetPartnerRepW9UpdateUrlReq(
                        null,
                        dummy.url
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerRepId required');

        });
        it('sets partnerRepId', () => {
            /*
             arrange
             */
            const expectedPartnerRepId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new GetPartnerRepW9UpdateUrlReq(
                    expectedPartnerRepId,
                    dummy.url
                );

            /*
             assert
             */
            const actualPartnerRepId =
                objectUnderTest.partnerRepId;

            expect(actualPartnerRepId).toEqual(expectedPartnerRepId);

        });
        it('throws if returnUrl is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new GetPartnerRepW9UpdateUrlReq(
                        dummy.userId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'returnUrl required');

        });
        it('sets returnUrl', () => {
            /*
             arrange
             */
            const expectedReturnUrl = dummy.url;

            /*
             act
             */
            const objectUnderTest =
                new GetPartnerRepW9UpdateUrlReq(
                    dummy.userId,
                    expectedReturnUrl
                );

            /*
             assert
             */
            const actualReturnUrl =
                objectUnderTest.returnUrl;

            expect(actualReturnUrl).toEqual(expectedReturnUrl);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new GetPartnerRepW9UpdateUrlReq(
                    dummy.userId,
                    dummy.url
                );

            const expectedObject =
            {
                partnerRepId: objectUnderTest.partnerRepId,
                returnUrl: objectUnderTest.returnUrl
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
