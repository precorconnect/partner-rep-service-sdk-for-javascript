import UpdatePartnerRepLastNameReq from '../../src/updatePartnerRepLastNameReq';
import dummy from '../dummy';

/*
 tests
 */
describe('UpdatePartnerRepLastNameReq class', () => {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdatePartnerRepLastNameReq(
                        null,
                        dummy.lastName
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');

        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new UpdatePartnerRepLastNameReq(
                    expectedId,
                    dummy.lastName
                );

            /*
             assert
             */
            const actualId =
                objectUnderTest.id;

            expect(actualId).toEqual(expectedId);

        });
        it('throws if lastName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdatePartnerRepLastNameReq(
                        dummy.userId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'lastName required');

        });
        it('sets lastName', () => {
            /*
             arrange
             */
            const expectedLastName = dummy.lastName;

            /*
             act
             */
            const objectUnderTest =
                new UpdatePartnerRepLastNameReq(
                    dummy.userId,
                    expectedLastName
                );

            /*
             assert
             */
            const actualLastName =
                objectUnderTest.lastName;

            expect(actualLastName).toEqual(expectedLastName);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new UpdatePartnerRepLastNameReq(
                    dummy.userId,
                    dummy.lastName
                );

            const expectedObject =
            {
                id: objectUnderTest.id,
                lastName: objectUnderTest.lastName
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
