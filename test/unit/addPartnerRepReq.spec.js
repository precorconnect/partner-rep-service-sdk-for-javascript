import AddPartnerRepReq from '../../src/addPartnerRepReq';
import dummy from '../dummy';

/*
 test methods
 */
describe('AddPartnerRepReq class', () => {
    describe('constructor', () => {
        it('throws if firstName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerRepReq(
                        null,
                        dummy.lastName,
                        dummy.emailAddress,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'firstName required');

        });
        it('sets firstName', () => {
            /*
             arrange
             */
            const expectedFirstName = dummy.firstName;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerRepReq(
                    expectedFirstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualFirstName =
                objectUnderTest.firstName;

            expect(actualFirstName).toEqual(expectedFirstName);

        });
        it('throws if lastName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerRepReq(
                        dummy.firstName,
                        null,
                        dummy.emailAddress,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'lastName required');

        });
        it('sets lastName', () => {
            /*
             arrange
             */
            const expectedLastName = dummy.lastName;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerRepReq(
                    dummy.firstName,
                    expectedLastName,
                    dummy.emailAddress,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualLastName =
                objectUnderTest.lastName;

            expect(actualLastName).toEqual(expectedLastName);

        });

        it('throws if emailAddress is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerRepReq(
                        dummy.firstName,
                        dummy.lastName,
                        null,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'emailAddress required');

        });
        it('sets emailAddress', () => {
            /*
             arrange
             */
            const expectedEmailAddress = dummy.emailAddress;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerRepReq(
                    dummy.firstName,
                    dummy.lastName,
                    expectedEmailAddress,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualEmailAddress =
                objectUnderTest.emailAddress;

            expect(actualEmailAddress).toEqual(expectedEmailAddress);

        });

        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerRepReq(
                        dummy.firstName,
                        dummy.lastName,
                        dummy.emailAddress,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(
                TypeError,
                'accountId required'
            );

        });
        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedAccountId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerRepReq(
                    dummy.firstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    expectedAccountId
                );

            /*
             assert
             */
            const actualAccountId =
                objectUnderTest.accountId;

            expect(actualAccountId).toEqual(expectedAccountId);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new AddPartnerRepReq(
                    dummy.firstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    dummy.accountId
                );

            const expectedObject =
            {
                firstName: objectUnderTest.firstName,
                lastName: objectUnderTest.lastName,
                emailAddress: objectUnderTest.emailAddress,
                accountId: objectUnderTest.accountId
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
